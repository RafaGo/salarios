package aplicação;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import entidades.FuncSalario;

public class Programa { 

    public static void main(String[]args){

        Locale.setDefault(Locale.US);
        Scanner ler = new Scanner(System.in); 

        List<FuncSalario> list = new ArrayList<>();

        System.out.print("Quantos Funcinarios quer registrar : ");
        int n = ler.nextInt();
        for(int i=0; i<n; i++){
            System.out.println("Funcionario# " + (i+1) + ":" );
            System.out.print("Id: ");
            Integer id = ler.nextInt();
            while (temId(list,id)) { 
                System.out.println("Id já existe, tente outro id ");
                id = ler.nextInt();
            }

            System.out.print("Nome: " );
            ler.nextLine();
            String nome = ler.nextLine();
            System.out.print("Salario: ");
            double salario = ler.nextDouble();
            FuncSalario funcionaro = new FuncSalario(id, nome, salario);
            list.add(funcionaro);
             } 

            System.out.println("Entre com id do funcionario que vai receber aumento de salario");
            Integer idSalario = ler.nextInt(); 
            Integer pos = posicao(list,idSalario);
            if(pos==null){
             System.out.println("Esse id não existe");   
            } 
            else {
             System.out.print("Entre com a porcentagem :");
             double percentual = ler.nextDouble();
             list.get(pos).AddSalario(percentual);
                  } 

            System.out.println();
            System.out.println("Lista de funcionarios: ");
            for (FuncSalario funcionario : list)
            System.out.println(funcionario);

            ler.close();
        }
            public static Integer posicao(List<FuncSalario> list, int id){
             for (int i=0; i < list.size(); i++){
               if (list.get(i).getId() == id){
                  return i;
                    }
                } 
                    return null;
                    } 
                    
            public static boolean temId(List<FuncSalario> list, int id ){
                FuncSalario funci = list.stream().filter(x -> x.getId() == id).findFirst().orElse(null);
               return funci != null;
              }      
        }           
